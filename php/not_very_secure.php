<?php 
/**
 * CODEWARS CHALLENGE
 *
 * In this example you have to validate if a user input string is alphanumeric. The given string is not nil, so you don't have to check that.
 *
 * The string has the following conditions to be alphanumeric:
 *
 * At least one character ("" is not valid)
 * Allowed characters are uppercase / lowercase latin letters and digits from 0 to 9
 * No whitespaces/underscore
 * BUGSREGULAR EXPRESSIONSDECLARATIVE PROGRAMMINGADVANCED LANGUAGE FEATURESFUNDAMENTALSSTRINGS
 *
 * class AlphanumericTest extends TestCase {
 *   public function testExamples() {
 *       $this->assertTrue(alphanumeric('Mazinkaiser'));
 *           $this->assertFalse(alphanumeric('hello world_'));
 *               $this->assertTrue(alphanumeric('PassW0rd'));
 *                   $this->assertFalse(alphanumeric('     '));
 *   }
 * }
 *
 * */

function alphanumeric(string $s): bool 
{
  return $s !== '' && preg_match("/([\s\W_])/", $s) === 0;
}

?>
