/*
CODEWARS CHALLENGE

Write simple .camelCase method (camel_case function in PHP, CamelCase in C# or camelCase in Java) for strings. All words must have their first letter capitalized without spaces.

For instance:

'hello case'.camelcase => HelloCase
'camel case word'.camelcase => CamelCaseWord
Don't forget to rate this kata! Thanks :)

ALGORITHMSFUNDAMENTALSSTRINGS
*/

<?php

function camel_case(string $s): string {
  $s = array_filter(explode(' ', $s), 'strlen');
  $buffer = '';
  foreach ($s as $val) {
    $buffer .= ucfirst($val);
  }
  return $buffer;
}