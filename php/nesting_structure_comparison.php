/*
CODEWARS CHALLENGE

Complete the function/method (depending on the language) to return true/True when its argument is an array that has the same nesting structure as the first array.

For example:

 // should return true
[ 1, 1, 1 ].sameStructureAs( [ 2, 2, 2 ] );          
[ 1, [ 1, 1 ] ].sameStructureAs( [ 2, [ 2, 2 ] ] );  

 // should return false 
[ 1, [ 1, 1 ] ].sameStructureAs( [ [ 2, 2 ], 2 ] );  
[ 1, [ 1, 1 ] ].sameStructureAs( [ [ 2 ], 2 ] );  

// should return true
[ [ [ ], [ ] ] ].sameStructureAs( [ [ [ ], [ ] ] ] ); 

// should return false
[ [ [ ], [ ] ] ].sameStructureAs( [ [ 1, 1 ] ] );
For your convenience, there is already a function 'isArray(o)' declared and defined that returns true if its argument is an array, false otherwise.

ALGORITHMSARRAYS
*/

<?php

function same_structure_as(array $a, array $b): bool {
  $checkStructure = function ($a, $b, $index = 0) use (&$checkStructure) {
        if (isset($a[$index]) || isset($b[$index])) {
            if (gettype($a) === gettype($b)) {
               if (isset($a[$index]) && is_array($a[$index])|| 
                     isset($b[$index]) && is_array($b[$index])) {
                    if ($checkStructure($a[$index], $b[$index], 0)) {
                        $index++;
                        return $checkStructure($a, $b, $index);
                    } else {
                        return false;
                    }
                } else {
                    $index++;
                    return $checkStructure($a, $b, $index);
                }
            } else {
                return false;
            }
        }
      return count($a) == count($b);
    };
  
  return $checkStructure($a, $b);
}