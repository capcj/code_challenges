/*
CODEWARS CHALLENGE

This is the first part of this kata series. Second part is here.

We want to create a simple interpreter of assembler which will support the following instructions:

mov x y - copies y (either a constant value or the content of a register) into register x
inc x - increases the content of register x by one
dec x - decreases the content of register x by one
jnz x y - jumps to an instruction y steps away (positive means forward, negative means backward), but only if x (a constant or a register) is not zero
Register names are alphabetical (letters only). Constants are always integers (positive or negative).

Note: the jnz instruction moves relative to itself. For example, an offset of -1 would continue at the previous instruction, while an offset of 2 would skip over the next instruction.

The function will take an input list with the sequence of the program instructions and will return a dictionary with the contents of the registers.

Also, every inc/dec/jnz on a register will always be followed by a mov on the register first, so you don't need to worry about uninitialized registers.

Example
simple_assembler(['mov a 5','inc a','dec a','dec a','jnz a -1','inc a'])

''' visualized:
mov a 5
inc a
dec a
dec a
jnz a -1
inc a
''''
The above code will:

set register a to 5,
increase its value by 1,
decrease its value by 2,
then decrease its value until it is zero (jnz a -1 jumps to the previous instruction if a is not zero)
and then increase its value by 1, leaving register a at 1
So, the function should return

{'a': 1}
This kata is based on the Advent of Code 2016 - day 12

ALGORITHMS INTERPRETERS
*/

<?php

function simple_assembler($program)
{
	$memory = [];
	$instruction = 0;
	$prog_size = count($program);
	$features = [
		"mov" => function(...$command) use (&$memory) {
			$command = $command[0];
			$memory[$command[1]] = is_numeric($command[2]) ? (int) $command[2] : $memory[$command[2]];
		},
		"inc" => function(...$command) use (&$memory) {
			$command = $command[0];
			$memory[$command[1]]++;
		},
		"dec" => function(...$command) use (&$memory) {
			$command = $command[0];
			$memory[$command[1]]--;
		},
		"jnz" => function(...$command) use (&$memory, &$instruction) {
			$command = $command[0];
			$comparator = is_numeric($command[1]) ? (int) $command[1] : $memory[$command[1]];
			if ($comparator !== 0) {
				$instruction += $command[2] - 1;
			}
		}
	];

	$program = array_map(function($command){ return explode(" ", $command); }, $program);

	while ($instruction < $prog_size) {
		$features[$program[$instruction][0]]($program[$instruction]);
		$instruction++;
	}

	return $memory;
}

/*
array(11) {
  [0]=>
  string(8) "mov c 12"
  [1]=>
  string(7) "mov b 0"
  [2]=>
  string(9) "mov a 200"
  [3]=>
  string(5) "dec a"
  [4]=>
  string(5) "inc b"
  [5]=>
  string(8) "jnz a -2"
  [6]=>
  string(5) "dec c"
  [7]=>
  string(7) "mov a b"
  [8]=>
  string(8) "jnz c -5"
  [9]=>
  string(7) "jnz 0 1"
  [10]=>
  string(7) "mov c a"
}

["mov c 12", "mov b 0", "mov a 200", "dec a", "inc b", "jnz a -2", "dec c", "mov a b", "jnz c -5", "jnz 0 1", "mov c a"]

*/