<?php
/*
	CODEWARS CHALLENGE

	https://www.codewars.com/kata/mtv-cribs/train/php

	Task:
Given n representing the number of floors build a beautiful multi-million dollar mansions like the ones in the example below:


     /\
    /  \
   /    \
  /______\  // number of floors 3
  |      |
  |      |
  |______|

     /\
    /  \
   /____\
   |    |   // 2 floors
   |____|

     /\
    /__\    // 1 floor
    |__|




Note: whitespace should be preserved on both sides of the roof. Number of floors will go up to 30. There will be no tests with invalid input.
Good luck!
FUNDAMENTALSCONTROL FLOWBASIC LANGUAGE FEATURESLOOPS
*/

function my_crib($n) {
  $mansion = "";

  $make_roof = function($n) use (&$mansion) {
  	$structure = function($space_i, $space_o) {
  		return str_replace(
  			['%space_o%', '%space_i%'],
  			[$space_o, $space_i],
  			'%space_o%/%space_i%\\%space_o%\n'
  		);
  	};
  	$inside = 0;
  	$end_roof = str_repeat('_', $n*2);
  	while($n) {
  		$spaces_outside = str_repeat(' ', $n);
  		$spaces_inside = str_repeat(' ', $inside);
  		$mansion .= $structure($spaces_inside, $spaces_outside);
  		$n--;
  		$inside += 2;
  	}
  	$mansion .= str_replace('%end_roof%', $end_roof,'/%end_roof%\\\n');
  };

  $make_body = function($n) use (&$mansion) {
  	$structure = function($space_i) {
  		return str_replace('%space_i%', $space_i, '|%space_i%|\n');
  	};
  	$floor = str_repeat('_', $n*2);
  	$width = $n * 2;
	while($n > 1) {
  		$spaces_inside = str_repeat(' ', $width);
  		$mansion .= $structure($spaces_inside);
  		$n--;
  	}
  	$mansion .= str_replace('%floor%', $floor, '|%floor%|');
  };

  $make_roof($n);
  $make_body($n);

  return $mansion;
}

/*
class MyTestCases extends TestCase {
    public function testBasics() {
      $this->assertEquals(' /\\ \n/__\\\n|__|', my_crib(1));
      $this->assertEquals('  /\\  \n /  \\ \n/____\\\n|    |\n|____|', my_crib(2));
      $this->assertEquals('   /\\   \n  /  \\  \n /    \\ \n/______\\\n|      |\n|      |\n|______|', my_crib(3));
    }
}
*/