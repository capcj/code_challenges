/*
CODEWARS CHALLENGE

Given a positive number n > 1 find the prime factor decomposition of n. The result will be a string with the following form :

 "(p1**n1)(p2**n2)...(pk**nk)"
with the p(i) in increasing order and n(i) empty if n(i) is 1.

Example: n = 86240 should return "(2**5)(5)(7**2)(11)"
FUNDAMENTALSNUMBERSMATHEMATICSALGORITHMSUTILITIES
*/

<?php

function primeFactors($n) {
    $primes = [];
    $calcPrime = function ($n, $i) use (&$primes) {
      while($n % $i == 0) {
        $primes[$i]++;
        $n /= $i;
      }
      return $n;
    };
    $n = $calcPrime($n, 2);
    for ($i = 3; $i <= sqrt($n); $i = $i + 2) {
        $n = $calcPrime($n, $i);
    }
    if ($n > 1) {
      $primes[$n]++;
    }
    $buffer = '';
    foreach ($primes as $prime => $exp) {
      $buffer .= "({$prime}";
      if ($exp > 1) {
        $buffer .= "**{$exp}";
      }
      $buffer .= ")";
    }
    return $buffer;
}
