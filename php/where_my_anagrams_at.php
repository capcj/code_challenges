/**
 *
 *
 * What is an anagram? Well, two words are anagrams of each other if they both contain the same letters. For example:
 *
 * 'abba' & 'baab' == true
 *
 * 'abba' & 'bbaa' == true
 *
 * 'abba' & 'abbba' == false
 *
 * 'abba' & 'abca' == false
 * Write a function that will find all the anagrams of a word from a list. You will be given two inputs a word and an array with words. You should return an array of all the anagrams or an empty array if there are none. For example:
 *
 * anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa']
 *
 * anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer']
 *
 * anagrams('laser', ['lazing', 'lazy',  'lacer']) => []
 * ALGORITHMSSTRINGS
 *
 * */
<?php 
final class AnagramsTest extends TestCase {
  public function testExamples() {
    $this->assertEquals(['a'], anagrams('a', ['a', 'b', 'c', 'd']));
    $this->assertEquals(['carer', 'arcre', 'carre'], anagrams('racer', ['carer', 'arcre', 'carre', 'racrs', 'racers', 'arceer', 'raccer', 'carrer', 'cerarr']));
    $this->assertEquals(['aabb', 'bbaa'], anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']), 'Your function should work for an example provided in the Kata Description');
    $this->assertEquals(['carer', 'racer'], anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']), 'Your function should work for an example provided in the Kata Description');
    $this->assertEquals([], anagrams('laser', ['lazing', 'lazy',  'lacer']), 'Your function should work for an example provided in the Kata Description');
  }
}

function anagrams(string $word, array $words): array {
  $w_chars = count_chars($word, 1);
  return array_reduce($words, function($result, $item) use ($w_chars) {
      if (count_chars($item, 1) == $w_chars) $result[] = $item;
      return $result;
    }) ?? [];
}
