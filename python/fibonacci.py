import sys

fibLimit = int(sys.argv[1]) if len(sys.argv) > 1 else 1

print("Fibonacci from ", fibLimit)

a, b = 1, 2
fibLimit -= 3

while (fibLimit):
  a, b = b, a + b
  fibLimit -= 1;

print(b)
