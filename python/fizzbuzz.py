def fizzbuzz(numbers):
    return [
            (((n % 3 == 0) * "Fizz") + ((n % 5 == 0) * "Buzz") or n) for n in numbers
        ]
        
print(fizzbuzz(range(1, 100)))
