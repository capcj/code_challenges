/*
CODEWARS CHALLENGE

Find the number with the most digits.

If two numbers in the argument array have the same number of digits, return the first one in the array.

FUNDAMENTALSNUMBERSSTRINGS
*/

#include <stddef.h>

int find_longest(int *numbers, size_t numbers_size)
{
    char c_int[12];
    for (int i = 1; i < numbers_size; i++) {
      if (sprintf(c_int, "%d", abs(numbers[i])) > sprintf(c_int, "%d", abs(numbers[0]))) {
        numbers[0] = numbers[i];
      }
    }
    return numbers[0];
}