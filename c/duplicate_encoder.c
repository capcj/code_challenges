/*
CODEWARS CHALLENGE

The goal of this exercise is to convert a string to a new string where each character in the new string is '(' if that character appears only once in the original string, or ')' if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.

Examples:

"din" => "((("

"recede" => "()()()"

"Success" => ")())())"

"(( @" => "))(("


Notes:

Assertion messages may be unclear about what they display in some languages. If you read "...It Should encode XXX", the "XXX" is actually the expected result, not the input! (these languages are locked so that's not possible to correct it).

FUNDAMENTALSSTRINGSARRAYS
*/

char* DuplicateEncoder(char* str)
{
  int str_len = strlen(str);
  char* str_encoded = malloc(sizeof(char) * str_len + 1);
  for (int i = 0; i < str_len; i++) {
    for (int j = 0; j < str_len; j++) {
      if (i != j && toupper(str[i]) == toupper(str[j])) {
        str_encoded[i] = ')';
        break;
      }
    }
    if (str_encoded[i] != ')') {
      str_encoded[i] = '(';
    }
  }
  str_encoded[str_len] = '\0';
  return  str_encoded;
}