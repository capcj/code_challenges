/*
CODEWARS CHALLENGE

Your task is to write a function toLeetSpeak that converts a regular english sentence to Leetspeak.

More about LeetSpeak You can read at wiki -> https://en.wikipedia.org/wiki/Leet

Consider only uppercase letters (no lowercase letters, no numbers) and spaces.

For example:

toLeetSpeak("LEET") returns "1337"
In this kata we use a simple LeetSpeak dialect. Use this alphabet:

{
  A : '@',
  B : '8',
  C : '(',
  D : 'D',
  E : '3',
  F : 'F',
  G : '6',
  H : '#',
  I : '!',
  J : 'J',
  K : 'K',
  L : '1',
  M : 'M',
  N : 'N',
  O : '0',
  P : 'P',
  Q : 'Q',
  R : 'R',
  S : '$',
  T : '7',
  U : 'U',
  V : 'V',
  W : 'W',
  X : 'X',
  Y : 'Y',
  Z : '2'
}

FUNDAMENTALS
*/

char *toLeetSpeak (char *speak) 
{
  char *leet_dict[121];
  int speak_len = strlen(speak);
  char *speak_encoded = malloc(sizeof(char) * speak_len + 1);
  int position = 0;
  leet_dict[65] = '@';
  leet_dict[66] = '8';
  leet_dict[67] = '(';
  leet_dict[68] = 'D';
  leet_dict[69] = '3';
  leet_dict[70] = 'F';
  leet_dict[71] = '6';
  leet_dict[72] = '#';
  leet_dict[73] = '!';
  leet_dict[74] = 'J';
  leet_dict[75] = 'K';
  leet_dict[76] = '1';
  leet_dict[77] = 'M';
  leet_dict[78] = 'N';
  leet_dict[79] = '0';
  leet_dict[80] = 'P';
  leet_dict[81] = 'Q';
  leet_dict[82] = 'R';
  leet_dict[83] = '$';
  leet_dict[84] = '7';
  leet_dict[85] = 'U';
  leet_dict[86] = 'V';
  leet_dict[87] = 'W';
  leet_dict[88] = 'X';
  leet_dict[89] = 'Y';
  leet_dict[90] = '2';
  for (int i = 0; i < speak_len; i++) {
    position = speak[i];
    if (position > 64 && position < 91) {
      speak_encoded[i] = leet_dict[position];
    } else {
      speak_encoded[i] = speak[i];
    }
  }
  speak_encoded[speak_len] = '\0';
  return speak_encoded;
}