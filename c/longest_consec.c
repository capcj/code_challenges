//n is strarr size
char* longestConsec(char* strarr[], int n, int k) {
  if (n == 0 || k > n || k <= 0) {
    return "";
  }
  int i;
  int result;
  char* longest_buffer[100];
  char* longest[100];
  strcpy(longest, "");
  while (k) {
    strcpy(longest_buffer, "");
    for (i = 0; i < n; i++) {
      result = strcmp(longest_buffer, strarr[i]);
      if (result == 0) {
        strcpy(strarr[i], "");
      } else if (result > 0) {
        strcpy(longest_buffer, strarr[i]);
        strcpy(strarr[i], "");
      }
    }
    strcat(longest, longest_buffer);
    strcpy(longest_buffer, "");
    k--;
  }
  
  return longest;
  
}

//=========================================================

//BEST CODEWARS SOLUTION

char* longestConsec(char* strarr[], int n, int k) {
    if ((n == 0) || (k > (int) n) || (k <= 0))
        return "";
    int maxSum = 0, start = 0, nd = 0;
    for (int i = 0; i <= n - k; i++) {
        int sum = 0;
        for (int j = i; j < i + k; j++)
          sum += strlen(strarr[j]);
        if (sum > maxSum) {
          maxSum = sum; start = i; nd = i + k;
        }
    }
    char* u = malloc(sizeof(char) * maxSum + 1);
    u[0] = '\0';
    for (int c = start; c < nd; c++)
        strcat(u, strarr[c]);
    return u;
}