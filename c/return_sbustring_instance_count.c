/*
CODEWARS CHALLENGE

Return substring instance count - 2

Complete the solution so that it returns the number of times the search_text is found within the full_text.

search_substr( fullText, searchText, allowOverlap = true )
so that overlapping solutions are (not) counted. If the searchText is empty, it should return 0. Usage examples:

search_substr('aa_bb_cc_dd_bb_e', 'bb') # should return 2 since bb shows up twice
search_substr('aaabbbcccc', 'bbb') # should return 1
search_substr( 'aaa', 'aa' ) # should return 2
search_substr( 'aaa', '' ) # should return 0
search_substr( 'aaa', 'aa', false ) # should return 1
ALGORITHMSSTRINGSREGULAR EXPRESSIONSDECLARATIVE PROGRAMMINGADVANCED LANGUAGE FEATURESFUNDAMENTALSSEARCH

TESTS

#include <stdbool.h>
#include <criterion/criterion.h>

int search_substr(const char *full_text, const char *search_text, bool allow_overlap);

Test(search_substr, should_pass_some_fixed_tests) {
    cr_assert_eq(search_substr("abcabcabc", "abc", false), 3);
    cr_assert_eq(search_substr("abcabcabc", "abc", true), 3);
    cr_assert_eq(search_substr("ababa_ababa_ababa", "aba", false), 3);
    cr_assert_eq(search_substr("ababa_ababa_ababa", "aba", true), 6);
}

*/

#include <stdbool.h>

int search_substr(const char *full_text, const char *search_text, bool allow_overlap) {
    int search_size = strlen(search_text);
    
    if (!search_size) {
      return 0;
    }
    
    int index = 0;
    int index2 = 0;
    int last_index = 0;
    int first_index = 0;
    bool buffer = true;
    int result = 0;
    int result_over = 0;
    int index_size = search_size - 1;
    while (full_text[index] != '\0') {
      buffer &= full_text[index] == search_text[index2];
      if (!allow_overlap && !buffer) {
        index += index_size - index2;
        continue;
      }
      if (index2 == 0) {
        first_index = index;
      }
      if (index2 == index_size) {
        if (buffer) {
          if (last_index != 0 && last_index == first_index) {
            result_over++;
            last_index = 0;
          } else if (!allow_overlap && index >= last_index + index_size || allow_overlap) {
            last_index = index;  
            result++;
          }
        }
        buffer = true;
        index2 = 0;
        index -= index_size;
      } else {
        index2++;
      }
      index++;
    }

    if (allow_overlap) result += result_over;
    
    return result;
}
