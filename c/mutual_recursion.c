/*
CODEWARS CHALLENGE

Mutual Recursion allows us to take the fun of regular recursion (where a function calls itself until a terminating condition) and apply it to multiple functions calling each other!

Let's use the Hofstadter Female and Male sequences to demonstrate this technique. You'll want to create two functions F and M such that the following equations are true:

F(0) = 1
M(0) = 0
F(n) = n - M(F(n - 1))
M(n) = n - F(M(n - 1))
Don't worry about negative numbers, n will always be greater than or equal to zero.

Hofstadter Wikipedia Reference http://en.wikipedia.org/wiki/Hofstadter_sequence#Hofstadter_Female_and_Male_sequences

ALGORITHMSMATHEMATICSNUMBERSRECURSIONCOMPUTABILITY THEORYTHEORETICAL COMPUTER SCIENCE

// TODO: Replace examples and use TDD development by writing your own tests. The code provided here is just a how-to example.

#include <criterion/criterion.h>

Test(F_function, should_pass_all_the_tests_F) {
    cr_assert_eq(1, F(0));
    cr_assert_eq(3, F(5));
    cr_assert_eq(6, F(10));
    cr_assert_eq(9, F(15));
    cr_assert_eq(16, F(25));
}

Test(M_Function, should_pass_all_the_tests_M) {
    cr_assert_eq(0, M(0));
    cr_assert_eq(3, M(5));
    cr_assert_eq(6, M(10));
    cr_assert_eq(9, M(15));
    cr_assert_eq(16, M(25));
}

*/

int F(int n) {
  return (n == 0) ? 1 : n - M(F(n - 1));
}

int M(int n) {
  return (n == 0) ? 0 : n - F(M(n - 1));
}