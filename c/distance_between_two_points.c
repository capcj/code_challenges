/*
Write a C program to calculate the distance between the two points.
Test Data :
Input x1: 25 
Input y1: 15 
Input x2: 35 
Input y2: 10 
Expected Output:
Distance between the said points: 11.1803 
*/

#include <stdio.h>
#include <math.h>

struct Point {
	int x, y;
};

void capture_points(struct Point *point, struct Point *point2);
double calc_distance_between(struct Point point, struct Point point2);

int main(void)
{
    struct Point point, point2;

    capture_points(&point, &point2);

    printf("Distance between the said points: %.4lf", calc_distance_between(point, point2));

    return 0;
}

void capture_points(struct Point *point, struct Point *point2)
{
    puts("Enter the first point x distance:");
    scanf("%d", &point->x);
    puts("Enter the first point y distance:");
    scanf("%d", &point->y);
    puts("Enter the second point x distance:");
    scanf("%d", &point2->x);
    puts("Enter the second point y distance:");
    scanf("%d", &point2->y);
}

double calc_distance_between(struct Point point, struct Point point2)
{
    return sqrt(pow((point.x - point2.x), 2) + pow((point.y - point2.y), 2));
}