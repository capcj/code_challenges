/*
 * CODEWARS CHALLENGES
 *
 *Chess Fun #1: Chess Board Cell Color
 *
 *Task

 *Given two cells on the standard chess board, determine whether they have the same color or not.
 *Example
 *
 *For cell1 = "A1" and cell2 = "C3", the output should be true.
 *
 *For cell1 = "A1" and cell2 = "H3", the output should be false.
 *
 *Input/Output
 *
 *[input] string cell1
 *
 *[input] string cell2
 *
 *[output] a boolean value
 *
 *true if both cells have the same color, false otherwise.
 *
 *]]
 *
 * */
#include <stdbool.h>

bool chess_board_cell_color(const char *cell1, const char *cell2)
{
    int cell1_t[2];
    int cell2_t[2];
    cell1_t[0] = ((cell1[0] - '0') - 16) % 2 == 0;
    cell1_t[1] = cell1[1] % 2 == cell1_t[0];
    cell2_t[0] = ((cell2[0] - '0') - 16) % 2 == 0;
    cell2_t[1] = cell2[1] % 2 == cell2_t[0];
    return cell1_t[1] == cell2_t[1];
}
