/*
 *
 * CODEWARS CHALLENGES
 *
 * Write a function that takes an (unsigned) integer as input, and returns the number of bits that are equal to one in the binary representation of that number.
 * Example: The binary representation of 1234 is 10011010010, so the function should return 5 in this case
 * Algorithms
 * Bits
 * binary
 *
 *
 * */
#include <stddef.h>

size_t countBits(unsigned value) {
  int total = 0;
  while (value) {
      total += value % 2; 
      value = value / 2;
    }
  return total;
}
